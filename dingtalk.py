#!/usr/bin/python3

import requests
import json

# 钉钉机器人的Webhook地址
webhook = 'https://oapi.dingtalk.com/robot/send?access_token=2d2125d21b4709382212e6eaccd614750a4a1951cb0c59cb4ad3d3bca097f5f7'

# 要发送的消息内容
message = {
    "msgtype": "text",
    "text": {
        "content": "KDE Crowdin - SVN 同步失败"
    }
}

# 发送POST请求到钉钉机器人Webhook
response = requests.post(webhook, json=message)

# 打印响应结果
print(response.text)
