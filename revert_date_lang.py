#!/usr/bin/python3

import sys

po_file_path = sys.argv[1]
po_file = open(po_file_path, 'r')
po_lines = po_file.readlines()
po_file.close()

diff_file_path = po_file_path + '.diff'
diff_file = open(diff_file_path, 'r')
diff_lines = diff_file.readlines()
diff_file.close()

should_revert_po_date = False
should_revert_pot_date = False
should_revert_lang = False
original_po_date = ''
original_pot_date = ''
original_lang = ''

for line in diff_lines:
    if line.startswith('-"PO-Revision-Date: '):
        original_po_date = line[1:]
        should_revert_po_date = True
    elif line.startswith('-"POT-Creation-Date: '):
        original_pot_date = line[1:]
        should_revert_pot_date = True
    elif line.startswith('-"Language: '):
        original_lang = line[1:]
        should_revert_lang = True

if should_revert_po_date and original_po_date:
    for i in range(len(po_lines)):
        line = po_lines[i]
        if line.startswith('"PO-Revision-Date:'):
            po_lines[i] = original_po_date
            break
        if (i > 20):
            break

if should_revert_pot_date and original_pot_date:
    for i in range(len(po_lines)):
        line = po_lines[i]
        if line.startswith('"POT-Creation-Date:'):
            po_lines[i] = original_pot_date
            break
        if (i > 20):
            break

if should_revert_lang and original_lang:
    for i in range(20):
        line = po_lines[i]
        if line.startswith('"Language-Team: '):
            next_line = po_lines[i+1]
            if not next_line.startswith('"Language: '):
                po_lines.insert(i + 1, original_lang)
        elif line.startswith('"Language: '):
            previous_line = po_lines[i-1]
            if not previous_line.startswith('"Language-Team: '):
                po_lines.pop(i)

po_file = open(po_file_path, 'w')
po_file.writelines(po_lines)
po_file.close()
